<?php
/**
 * Created by PhpStorm.
 * User: aszymczyk
 * Date: 12/12/17
 * Time: 1:12 PM
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Station
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\Table(name="station")
 */
class Station
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

}